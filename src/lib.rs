extern crate rand;

enum Op {
    Addition(i32, i32),
    Substraction(i32, i32),
    Multiplication(i32, i32),
    Quotient(i32, i32),
    Remainder(i32, i32),
}

fn result(op: Op) -> i32 {
    match op {
        Op::Addition(a, b) => a + b,
        Op::Substraction(a, b) => a - b,
        Op::Multiplication(a, b) => a * b,
        Op::Quotient(a, b) => a / b,
        Op::Remainder(a, b) => a % b,
    }
}

pub fn run() {
    let a: i32 = rand::random();
    println!("{} was chosen at random", a);

    use std::os::unix::net;

    let listener = net::UnixListener::bind("/home/rdesplanques/test_socket").unwrap();
}
