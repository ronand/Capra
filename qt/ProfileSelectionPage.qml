import QtQuick 2.9
import QtQuick.Controls 2.2

Page {
    id: root

    header: ToolBar {
        ToolButton {
            text: "Back"
            onClicked: root.StackView.view.pop()
        }
    }

    ListView {
        anchors.fill: parent

        model: ["Pierre", "Paul", "Jacques"]

        delegate: ItemDelegate {
            text: modelData
            anchors.right: parent.right
            anchors.rightMargin: parent.width / 2 - 50
            onClicked: root.StackView.view.push("qrc:/MainPage.qml", { })
        }

        ScrollIndicator.vertical: ScrollIndicator { }
    }
}
