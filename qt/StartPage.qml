import QtQuick 2.9
import QtQuick.Controls 2.2

Page {
    id: root

    signal clicked()

    Button {
        anchors.centerIn: parent
        text: "Commencer"

        onClicked: root.clicked()
    }
}
