import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ApplicationWindow {
    property color backgroundColor: "#303030"

    visible: true
    width: 640
    height: 480

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: StartPage {
            onClicked: stackView.push("qrc:/ProfileSelectionPage.qml")
        }
    }
}
