#include "backend.h"

BackEnd::BackEnd(QObject *parent) : QObject(parent)
{
    generate();
}

QString BackEnd::opString()
{
    QString retVal;
    retVal.reserve(9);

    retVal.append(QString::number(a));

    switch (op) {
    case Op::Plus:
        retVal.append(" + ");
        break;
    case Op::Times:
        retVal.append(" × ");
    default:
        break;
    }

    retVal.append(QString::number(b));

    return retVal;
}

bool BackEnd::answer(QString answer)
{
    int ra;
    switch (op) {
    case Op::Plus:
        ra = a + b;
        break;
    case Op::Times:
        ra = a * b;
    default:
        break;
    }

    int a = answer.toInt();
    return ra == a;
}

void BackEnd::generate()
{
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<int> dis(0, 99);

    a = dis(gen);
    b = dis(gen);
    if (dis(gen) % 2 == 0) {
        op = Op::Plus;
    } else {
        op = Op::Times;
    }
}
