import QtQuick 2.9
import QtQuick.Controls 2.2

Rectangle {
    id: root

    property alias text: textField.text

    Label {
        id: textField
        anchors.centerIn: parent
    }

    states: [
        State {
            name: "right"
            PropertyChanges {
                target: root
                color: "green"
            }
        },
        State {
            name: "wrong"
            PropertyChanges {
                target: root
                color: "red"
            }
        }
    ]

    transitions: [
        Transition {
            ColorAnimation {
                duration: 200
            }
        }
    ]
}
