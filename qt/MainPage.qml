import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    ColumnLayout {
        visible: true
        anchors.fill: parent

        Prompt {
            id: prompt
            text: backEnd.opString()

            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        TextField {
            focus: true
            Layout.fillWidth: true

            validator: IntValidator {
                bottom: 0
                top: 10000
            }

            onAccepted: {
                var p = backEnd.answer(text);
                if (p) {
                    prompt.state = "right";
                    backEnd.generate();
                    prompt.text = backEnd.opString();
                } else {
                    prompt.state = "wrong";
                }
                text = "";
            }
        }
    }
}
