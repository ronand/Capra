#ifndef BACKEND_H
#define BACKEND_H

#include <random>

using std::random_device;
using std::mt19937;
using std::uniform_int_distribution;

#include <QObject>
#include <QString>

class BackEnd : public QObject
{
    Q_OBJECT
public:
    explicit BackEnd(QObject *parent = nullptr);

    Q_INVOKABLE QString opString();
    Q_INVOKABLE bool answer(QString answer);

private:
    enum class Op {
        Plus,
        Times,
    };

    int a;
    int b;
    Op op;

signals:

public slots:
    void generate();
};

#endif // BACKEND_H
