#include <QGuiApplication>
#include <QtQml>
#include <QUrl>

#include "backend.h"

int main(int argc, char *argv[]) {
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app { argc, argv };

    QQmlApplicationEngine engine;

    BackEnd be;

    engine.rootContext()->setContextProperty("backEnd", &be);

    engine.load( QUrl { QString { "qrc:/main.qml" } });

    if (engine.rootObjects().isEmpty()) {
        return EXIT_FAILURE;
    }
    return app.exec();
}
