import qbs

QtApplication {
    name: "Capra"
    files: [
        "backend.cpp",
        "backend.h",
        "main.cpp",
        "qml.qrc",
    ]

    Depends { name: "Qt.quick" }
}
