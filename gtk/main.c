#include <gtk/gtk.h>

static void print_message(GtkWidget *w, gpointer data) {
    g_print("Bienvenue dans Lecm\n");
}

static void activate(GtkApplication *app, gpointer user_data) {
    GtkWidget *window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Logiciel entraînement calcul mental");
    gtk_window_set_default_size(GTK_WINDOW(window), 640, 480);

    GtkWidget *grid = gtk_grid_new();
    
    gtk_container_add(GTK_CONTAINER(window), grid);

    GtkWidget *button = gtk_button_new_with_label("Commencer");
    g_signal_connect(button, "clicked", G_CALLBACK(print_message), NULL);

    gtk_grid_attach(GTK_GRID(grid), button, 0, 0, 1, 1);

    GtkWidget *button1 = gtk_button_new_with_label("Continuer");
    g_signal_connect(button1, "clicked", G_CALLBACK(print_message), NULL);

    gtk_grid_attach(GTK_GRID(grid), button1, 1, 0, 1, 1);

    GtkWidget *button2 = gtk_button_new_with_label("Finir");
    g_signal_connect_swapped(button2, "clicked", G_CALLBACK(gtk_widget_destroy), window);

    gtk_grid_attach(GTK_GRID(grid), button2, 0, 1, 2, 1);

    gtk_widget_show_all(window);
}

int main(int argc, char **argv) {
    GtkApplication *app = gtk_application_new("com.gitlab.ronand.lecm", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    const int status = g_application_run(G_APPLICATION(app), argc, argv);

    return status;
}
